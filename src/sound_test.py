from playsound import playsound
import winsound
from pydub import AudioSegment
from pydub.playback import play

sound_file = 'sound/background_sound.wav'
song = AudioSegment.from_wav(sound_file)
play(song)
# sound_file = 'sound/background_sound.wav'


playsound(sound_file)


frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second
# winsound.Beep(frequency, duration)
flags = winsound.SND_FILENAME | winsound.SND_ASYNC | winsound.SND_LOOP
# winsound.PlaySound(sound_file, flags)