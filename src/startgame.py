import pygame
from pygame.locals import *
import random

__author__    = "Author: PWLO"
__version__   = "Revision: 0.2"
__date__      = "Date: 2023-06-16 18:09:43 +0200 (Fr, 16 jun 2023)"
__license__   = 'public domain'
__copyright__ = "PWLO (c) 2023"

'''
TODO:
    - restart level if alien dead
    - exception handling
    - 2. Level: UFOs shoot back
    - 3. Level: more UFOs, UFOs come closer
'''

class rocket:
    def __init__(self, x_offset) -> None:
        self.rocket_xpos = 265
        self.rocket_xpos += x_offset # if alien moves
        self.rocket_ypos = 270
        self.rocket_step = 15

class bomb:
    def __init__(self, x_pos, y_pos) -> None:
        self.bomb_xpos = x_pos
        self.bomb_ypos = y_pos
        self.bomb_step = 15

class AlienShooter:

    def __init__(self) -> None:
        pygame.init()
        self.title = "Alien shooter"
        # pygame.mixer.pre_init(frequency=44100, size=-16, channels=4, buffer=512, devicename=None, allowedchanges=AUDIO_ALLOW_FREQUENCY_CHANGE | AUDIO_ALLOW_CHANNELS_CHANGE)
        pygame.mixer.init()
        sound_file = 'sound/doom.flac'
        # print(pygame.mixer.get_init())
        sound = pygame.mixer.Sound(sound_file)
        # sound.set_volume(0.5)
        # pygame.mixer.find_channel().play(sound)
        sound.play(-1)
        
        # load and set the logo
        logo = pygame.image.load("img/alien32x32.png")
        pygame.display.set_icon(logo)
        pygame.display.set_caption(self.title)

        self.screen_width = 580
        self.screen_height = 360
        self.screen = pygame.display.set_mode((self.screen_width,self.screen_height))
        # screen.fill((255,255,255))
        # image instead
        # self.bgd_image = pygame.image.load("img/mars_bgd.jpg")
        self.bgd_image = pygame.image.load("img/mars_bgd_1207x402.jpg")
        self.screen.blit(self.bgd_image, (0,0))

        # define the position of the ufos
        self.xpos = 50
        self.ypos = 50
        self.xpos_2 = 100
        self.ypos_2 = 100
        # how many pixels we move our ufo each frame
        self.step_x = 10
        self.step_y = 10
        self.step_x_2 = 10
        self.step_y_2 = 10

        # alien position
        self.alien_x = 250
        self.alien_y = 280

        # rockets
        self.rocket_list = []
        self.rocket_offset = 0

        # bombs
        self.bomb_list = []

        # explosion_pos
        self.explosion_x = 0
        self.explosion_y = 0      

        # alien image
        alien_img = pygame.image.load("img/alien.png")
        target_size = (50, 50)  # Width, Height
        self.resized_alien_img = pygame.transform.scale(alien_img, target_size)
        
        # dead alien image
        dead_alien_img = pygame.image.load("img/dead_alien.png")
        target_size = (50, 50)  # Width, Height
        self.resized_dead_alien_img = pygame.transform.scale(dead_alien_img, target_size)
        self.alien_dead = False

        # rocket image
        rocket_img = pygame.image.load("img/rocket_2.png")
        rocket_target_size = (20, 20)  # Width, Height
        self.resized_rocket_img = pygame.transform.scale(rocket_img, rocket_target_size)
        self.rocket_img_rect = self.resized_rocket_img.get_rect()

        # bomb image
        bomb_img = pygame.image.load("img/bomb.png")
        bomb_target_size = (30, 30)  # Width, Height
        self.resized_bomb_img = pygame.transform.scale(bomb_img, bomb_target_size)
        self.bomb_img_rect = self.resized_bomb_img.get_rect()

        # explosion image
        expl_img = pygame.image.load("img/explosion.png")
        expl_target_size = (40, 40)  # Width, Height
        self.resized_expl_img = pygame.transform.scale(expl_img, expl_target_size)
        # self.expl_img_rect = self.resized_expl_img.get_rect()
        
        # kaboom image
        kaboom_img = pygame.image.load("img/kaboom.png")
        kaboom_target_size = (100, 100)  # Width, Height
        self.resized_kaboom_img = pygame.transform.scale(kaboom_img, kaboom_target_size)

        # ufo images
        self.ufo_img_1 = pygame.image.load("img/misterious-ufo.png")
        self.ufo_img_2 = pygame.image.load("img/alienship.png")
        target_size = (50, 50)
        self.resized_ufo_1_img = pygame.transform.scale(self.ufo_img_1, target_size)
        self.resized_ufo_2_img = pygame.transform.scale(self.ufo_img_2, target_size)
        self.resized_ufo_1_img_rect = self.resized_ufo_1_img.get_rect()
        self.resized_ufo_2_img_rect = self.resized_ufo_2_img.get_rect()
        self.ufo_1_destroied = False
        self.ufo_2_destroied = False

        # score
        self.score = 0

        self.random_drop = random.randint(10, 570)

        pygame.display.flip()
        self.clock = pygame.time.Clock()

    def __repr__(self):
        return f"{self.title} {__version__}"
        
    def main_thread(self) -> None:
        running = True
        temp_ufo_1_img_rect = None
        ufo_1_shot, ufo_2_shot, explosion = 0, 0, 0
        # main loop
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == K_SPACE:
                        if not self.alien_dead:
                            self.rocket_list.append(rocket(self.rocket_offset))
                    elif event.key == pygame.K_LEFT:
                        if not self.alien_dead:
                            self.alien_x -= 5
                            self.rocket_offset -= 5 
                    elif event.key == pygame.K_RIGHT:
                        if not self.alien_dead:
                            self.alien_x += 5
                            self.rocket_offset += 5
                    elif event.key == pygame.K_s:
                        # Restart level
                        self.ufo_1_destroied = False
                        self.alien_dead = False
                        self.xpos = 50
                        self.ypos = 50
                        self.xpos_2 = 100
                        self.ypos_2 = 100
                        target_size = (50, 50)
                        self.resized_ufo_1_img = pygame.transform.scale(self.ufo_img_1, target_size)
                        self.resized_ufo_2_img = pygame.transform.scale(self.ufo_img_2, target_size)
                        self.resized_ufo_1_img_rect = self.resized_ufo_1_img.get_rect()
                        self.resized_ufo_2_img_rect = self.resized_ufo_2_img.get_rect()
                        self.resized_alien_img_rect = self.resized_alien_img.get_rect()

            # check if the ufo is still on screen, if not change direction
            if self.xpos > self.screen_width - 64 or self.xpos < 0:
                self.step_x = -self.step_x
                self.random_drop = random.randint(10, 570)
            # if ypos>self.screen_height-64 or ypos<0:
            #     step_y = -step_y
            if self.xpos_2 > self.screen_width - 64 or self.xpos_2 < 0:
                self.step_x_2 = -self.step_x_2
            # if ypos_2>self.screen_height-64 or ypos_2<0:
            #     step_y_2 = -step_y_2

            # update the position of the ufos
            self.xpos += self.step_x # move it to the right
            # ypos += step_y # move it down
            self.xpos_2 += self.step_x_2 # move it to the right
            # ypos_2 += step_y_2 # move it down

            if self.resized_ufo_1_img_rect is not None:
                self.resized_ufo_1_img_rect.topleft = (self.xpos, self.ypos)
            if self.resized_ufo_2_img_rect is not None:
                self.resized_ufo_2_img_rect.topleft = (self.xpos_2, self.ypos_2)

            self.screen.blit(self.bgd_image, (0,0))
            # check if alien dead
            if self.alien_dead:
                self.screen.blit(self.resized_dead_alien_img, self.resized_dead_alien_img_rect)
            else:
                self.resized_alien_img_rect = self.resized_alien_img.get_rect()
                self.resized_alien_img_rect.topleft = (self.alien_x,self.alien_y)
                self.screen.blit(self.resized_alien_img, self.resized_alien_img_rect)
            if self.resized_alien_img_rect.colliderect(self.bomb_img_rect):
                self.resized_dead_alien_img_rect = self.resized_dead_alien_img.get_rect()
                self.resized_dead_alien_img_rect.topleft = (self.alien_x,self.alien_y)
                self.screen.blit(self.resized_dead_alien_img, self.resized_dead_alien_img_rect)
                self.alien_dead = True
            
            # drop bomb
            if not self.ufo_1_destroied:
                if self.random_drop - 5 <= self.xpos <= self.random_drop + 5:
                    self.bomb_list.append(bomb(self.xpos, self.ypos + 25))
                if len(self.bomb_list) != 0:
                    for b in self.bomb_list:
                        if b.bomb_ypos > 300:
                            sound = pygame.mixer.Sound('sound/big-impact.mp3')
                            sound.play()
                            explosion = 10
                            self.explosion_x = b.bomb_xpos
                            self.explosion_y = b.bomb_ypos
                            self.bomb_list.remove(b)
                            b = None
                        else:
                            if b is not None:
                                self.bomb_img_rect.topleft = (b.bomb_xpos, b.bomb_ypos)
                                self.screen.blit(self.resized_bomb_img, self.bomb_img_rect)
                                b.bomb_ypos += b.bomb_step

            if self.resized_ufo_1_img is not None or self.resized_ufo_1_img_rect is not None:
                self.screen.blit(self.resized_ufo_1_img, self.resized_ufo_1_img_rect)
            if self.resized_ufo_2_img is not None or self.resized_ufo_2_img_rect is not None:
                self.screen.blit(self.resized_ufo_2_img, self.resized_ufo_2_img_rect)

            # start shooting
            for r in self.rocket_list:
                self.rocket_img_rect.topleft = (r.rocket_xpos, r.rocket_ypos)
                self.screen.blit(self.resized_rocket_img, self.rocket_img_rect)
                r.rocket_ypos -= r.rocket_step
                if self.resized_ufo_1_img_rect is not None:
                    if self.resized_ufo_1_img_rect.colliderect(self.rocket_img_rect):
                        self.ufo_1_destroied = True
                        sound = pygame.mixer.Sound('sound/explosion.mp3')
                        sound.play()
                        ufo_1_shot = 10
                        temp_ufo_1_img_rect = self.resized_ufo_1_img_rect
                        self.screen.blit(self.resized_kaboom_img, temp_ufo_1_img_rect)
                        self.resized_ufo_1_img = None
                        self.resized_ufo_1_img_rect = None
                        self.rocket_list.remove(r)
                if self.resized_ufo_2_img_rect is not None:
                    if self.resized_ufo_2_img_rect.colliderect(self.rocket_img_rect):
                        sound = pygame.mixer.Sound('sound/explosion.mp3')
                        sound.play()
                        ufo_2_shot = 10
                        temp_ufo_2_img_rect = self.resized_ufo_2_img_rect
                        self.screen.blit(self.resized_kaboom_img, temp_ufo_2_img_rect)
                        self.resized_ufo_2_img = None
                        self.resized_ufo_2_img_rect = None
                        self.rocket_list.remove(r)

            # make kaboom stay longer, alien jump, explosion
            if ufo_1_shot > 0:
                self.screen.blit(self.resized_kaboom_img, temp_ufo_1_img_rect)
                if ufo_1_shot <= 5 and ufo_1_shot > 1:
                    self.alien_y = 270
                if ufo_1_shot == 1:
                    self.alien_y = 280
                ufo_1_shot -= 1
            if ufo_2_shot > 0:
                self.screen.blit(self.resized_kaboom_img, temp_ufo_2_img_rect)
                if ufo_2_shot <= 5  and ufo_2_shot > 1:
                    self.alien_y = 270
                if ufo_2_shot == 1:
                    self.alien_y = 280
                ufo_2_shot -= 1
            if explosion > 0:
                self.screen.blit(self.resized_expl_img, (self.explosion_x, self.explosion_y))
                explosion -= 1
            pygame.display.flip()
            # this will slow it down to 10 fps, so you can watch it, 
            # otherwise it would run too fast
            self.clock.tick(10)
    
    def __del__(self):
        pygame.mixer.quit()
        pygame.quit()

# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    als = AlienShooter()
    print(als)
    als.main_thread()
