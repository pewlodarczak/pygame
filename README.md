# Simple pygame sample project

## Versions
```batch
Python: 3.x
pygame: 2.4
```

## Start game
```batch
python .\startgame.py
```

## Screenshots

<img src="src/img/Screenshot 2023-06-16 110528.png" alt="Screenshot" width="600">
<br><br>
<img src="src/img/Screenshot 2023-06-16 134559.png" alt="Screenshot" width="600">
